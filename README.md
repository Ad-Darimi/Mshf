
# Mshf

A repository for:
1. Mushaf of Al-Quran Al-Majeed, 
2. Two plain text files containing the interpretation of meaning of Quran, the first of which is by ***Sheikh Taqiuddin Hilali*** & ***Sheikh Muhsin Khan***, while the second is by *Sahih International*. The former is recommended over latter for more accuracy. 
3. HTML file for Sheikh Hilali & Khan's interpretation of the meaning, by Darussalam Publication

You can get verses of Quran as shell prompt (like bash or zsh) too. Append `printf '\n%s\n\n' "$(shuf -n1 <your chosen location>/mshf/txt-files/hilali.txt)"` or `echo; shuf -n 1 < <your chosen location>/mshf/txt/files/hilali.txt; echo` to your `.bashrc` or `.zshrc`
or add an alias for any of these two: `"alias ayah='echo; shuf -n 1 < <location>/mshf/txt/files/hilali.txt; echo'"`
I recommend [TheNobleQuran.com](http://thenoblequran.com/) by our ustad, Abu Iyaad Amjad Rafiq for english translated exegesis of various verses.  

### Downloading/Cloning
1. Clone the repository `git clone https://git.nixnet.services/Ad-Daraqutni/mshf.git`
2. Move the `mshf` folder to wherever you want. Like `mv mshf ~/.config/`. 
   - You'd need to edit `Mshf.html` at line nos. 6, 7 & 8, to suite that location.
  - The format is `file://<Absolute location>`
  - You can sed it: `sed 's/user9/<your $USER>/g' Mshf.html >> Mshf-mine.html`
3. Install the font KFGQPC Hafs font to make it easier to read: `cd mshf && mkdir -p /usr/share/fonts/misc/ && cp KFGQPC-Hafs.otf /usr/share/fonts/misc/`
  - To use your own preferred Arabic font style & size, edit [Mshf/cache/common-style.css] (https://codeberg.org/Ad-Darimi/Mshf/src/branch/master/cache/common-style.css)

### Samples
##### **Mshf in Arabic, with dark background**
![Mshf.html](https://codeberg.org/Ad-Darimi/Mshf/raw/branch/master/sample-screenshots/Mshf_html--scrot.png)
##### **Darussalam HTML**
![Darussalam's english text](https://codeberg.org/Ad-Darimi/Mshf/raw/branch/master/sample-screenshots/darussalam---hilali.png)
##### **Shell Prompt**
![Shell Prompt](https://codeberg.org/Ad-Darimi/Mshf/raw/branch/master/sample-screenshots/ayah-shell-prompt.png)

### Data sources:
1. [Tanzil](https://tanzil.net/)
2. [Dar-us-salam](https://darussalam.com/quran-mushaf/english-translation-quran/) 
